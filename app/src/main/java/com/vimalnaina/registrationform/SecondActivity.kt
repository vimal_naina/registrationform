package com.vimalnaina.registrationform

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val txtDetails = findViewById<TextView>(R.id.txtDetails)

        val intent = intent
        with(intent){
            val name = getStringExtra("name")
            val email = getStringExtra("email")
            val phone = getStringExtra("phone")
            val pass = getStringExtra("pass")

            ("Name:-  " + name + "\nEmail:-  " + email +
                    "\nPhoneNo:-  " + phone + "\nPassword:-  " + pass).also {txtDetails.text = it}
        }
    }
}