package com.vimalnaina.registrationform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txtName = findViewById<EditText>(R.id.etxtName)
        val txtEmail = findViewById<EditText>(R.id.etxtEmail)
        val txtPhone = findViewById<EditText>(R.id.etxtPhone)
        val txtPass = findViewById<EditText>(R.id.etxtPass)
        val txtConfirmPass = findViewById<EditText>(R.id.etxtConfirmPass)
        val register = findViewById<Button>(R.id.btnRegister)

        register.setOnClickListener(){
            val name = txtName.text.toString()
            val email = txtEmail.text.toString()
            val phone = txtPhone.text.toString()
            val pass = txtPass.text.toString()
            val confirmPass = txtConfirmPass.text.toString()
            if(pass.equals(confirmPass)){
                val intent = Intent(applicationContext,SecondActivity::class.java)
                intent.apply {
                    putExtra("name",name)
                    putExtra("email",email)
                    putExtra("phone",phone)
                    putExtra("pass",pass)
                    startActivity(intent)
                }
            }else{
                Snackbar.make(it,"Password & Confirm password should be same.",Snackbar.LENGTH_LONG).show()
            }
        }
    }
}